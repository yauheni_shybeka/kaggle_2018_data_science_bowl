# Description

[Kaggle Nuclei Data Science Bowl 2018](
	https://www.kaggle.com/c/data-science-bowl-2018)
Solver

---

## Overview

### Data

Execute `download.sh` script from `data` subdirectory. It will download latest
fixed images from [lopuhin repository](
	https://github.com/lopuhin/kaggle-dsbowl-2018-dataset-fixes.git)

This repository contains only train dataset.

You can use source data from kaggle:

* [stage1\_train](https://www.kaggle.com/c/8089/download/stage1_train.zip)
* [stage1\_test](https://www.kaggle.com/c/8089/download/stage1_test.zip)

### Model

Provided models:

- [x] UNet
- [ ] Mask-RCNN
- [ ] Windowed-UNet

### Training

#### UNet

The model uses **sparse softmax cross entropy** loss function for
multiclass data classification.

---

## How to use

### Dependencies

Please, see `requrements.txt`.

### Prepare the data

Please, see `preprocess_unet.sh` from `samples` subdirectory.

### Train the model

Please, see `train_unet.sh` from `samples` subdirectory.
