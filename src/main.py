from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import sys

import json

import utils.clparse
import utils.logwrap

import api.unet


logger = utils.logwrap.get_logger(__name__, root = True)

def _read_config(config_file):
    logger.info("The config file location: %s" %\
            os.path.abspath(config_file))
    assert os.path.isfile(config_file),\
            "Invalid config file location"

    with open(config_file, "r") as cfg_reader:
        config = json.loads(cfg_reader.read())
        logger.debug("Provided config: %s" % str(config))

        return config

def _get_api(config):
    if "model" not in config:
        raise ValueError("Invalid or corrupted config (model wasn't found)")

    if config["model"] == "unet":
        return api.unet
    else:
        raise ValueError("Undefined model %s" % config["model"])

def preprocess(args, config):
    api = _get_api(config)
    return api.preprocess(args, config["args"])

def train(args, config):
    api = _get_api(config)
    return api.train(args, config["args"])

def predict(args, config):
    api = _get_api(config)
    return api.predict(args, config["args"])

def main():
    args = utils.clparse.parse_args(
            preprocess_command = preprocess,
            train_command = train,
            predict_command = predict)
    
    if args.command != None:
        config = _read_config(args.config_file)
        args.command(args, config)
    else:
        print("No command is provided. Please, see main.py -h / --help",
                file = sys.stderr)

if __name__ == "__main__":
    main()
