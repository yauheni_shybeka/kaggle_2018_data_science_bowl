from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os

import tensorflow as tf
import numpy as np

from tqdm import tqdm

# TEMPORARY
import pandas as pd
import skimage.measure
import skimage.transform

import unet
import api.unet

import utils.logwrap


logger = utils.logwrap.get_logger(__name__)

def _check_input_dir(input_dir):
    logger.info("The input directory location: %s" %\
            os.path.abspath(input_dir))
    assert os.path.isdir(input_dir),\
            "Invalid input directory location"

def _check_models_root(models_root):
    logger.info("The root of models: %s" %\
            os.path.abspath(models_root))
    assert os.path.isdir(models_root),\
            "Invalid root of models location"

def _create_input_reader(input_dir, config):
    input_reader = unet.io.InputReader(input_dir, config["channels"])
    return input_reader

def _create_dataset_transform(config):
    dataset_transform = unet.io.DatasetTransform(
            image_height = config["image_height"],
            image_width = config["image_width"],
            num_channels = unet.helpers.num_channels(config["channels"]),
            num_classes = config["num_classes"])

    return dataset_transform

def _restore_model(sess, models_root, config):
    model = api.unet.share.build_model_graph(config)

    model_path = api.unet.share.restore_model_path(
            models_root,
            config)
    logger.debug("Restoring the model by path: %s" %\
            os.path.abspath(model_path))

    model_dir = api.unet.share.restore_model_dir(
            models_root,
            config)

    saver = tf.train.Saver()
    saver.restore(sess, tf.train.latest_checkpoint(model_dir + '/.'))

    return model

def _make_predictions(sess, input_reader, dataset_transform, model):
    predictor = unet.metrics.Predictor(model)

    image_ids = input_reader.get_ids()
    logger.debug("The number of test entries: %s" % len(image_ids))

    predictions = []

    for image_id in tqdm(image_ids):
        image = input_reader.load_image(image_id)
        x_image = dataset_transform.transform_image(image)
        x_net = np.expand_dims(x_image, axis = 0)

        prediction = predictor.predict(sess, x_net,
                out_size = [ image.shape[0], image.shape[1] ])[0]

        predictions.append(prediction)

    return predictions


# TODO refactor dump code
def _rle_encoding(x):
    dots = np.where(x.T.flatten() == 1)[0]
    run_lengths = []
    prev = -2
    for b in dots:
        if (b>prev+1): run_lengths.extend((b + 1, 0))
        run_lengths[-1] += 1
        prev = b
    return run_lengths

def _prob_to_rles(x, cutoff=0.5):
    lab_img = skimage.measure.label(x > cutoff)
    for i in range(1, lab_img.max() + 1):
        yield _rle_encoding(lab_img == i)

def _dump_file_name(dump_file, config):
    return "{}_unet_{}_{}_{}_{}_.csv".format(
            dump_file,
            config["channels"],
            config["image_height"],
            config["image_width"],
            config["num_classes"])

def _dump_results(input_reader, predictions, dump_file_name):
    image_ids = input_reader.get_ids()

    new_test_ids = []
    rles = []

    print ("RLE postprocessing:")
    for ordinal, prediction in\
            tqdm(enumerate(predictions), total = len(predictions)):
        rle = list(_prob_to_rles(prediction))
        rles.extend(rle)

        image_id = image_ids[ordinal]
        new_test_ids.extend([image_id] * len(rle))

    sub = pd.DataFrame()
    sub["ImageId"] = new_test_ids
    sub["EncodedPixels"] = pd.Series(rles)\
            .apply(lambda x: ' '.join(str(y) for y in x))

    sub.to_csv(dump_file_name, index = False)

def predict(args, config):
    _check_input_dir(args.input_dir)
    _check_models_root(args.models_root)

    input_reader = _create_input_reader(args.input_dir, config)
    dataset_transform = _create_dataset_transform(config)

    with tf.Session(graph = tf.Graph()) as sess:
        model = _restore_model(sess, args.models_root, config)
        predictions =\
                _make_predictions(sess, input_reader, dataset_transform, model)

        dump_file_name = _dump_file_name(args.dump_file, config)
        _dump_results(input_reader, predictions, dump_file_name)
