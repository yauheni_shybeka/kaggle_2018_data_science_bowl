from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os

import unet


def restore_dataset_name(config):
    name = "unet_images_%s_%d_%d_%d" % (
            config["channels"],
            config["image_height"],
            config["image_width"],
            config["num_classes"])
    return name

def restore_dataset_path(datasets_root, config):
    dataset_name = restore_dataset_name(config)
    dataset_path = os.path.join(datasets_root,
            dataset_name)

    return dataset_path

def restore_model_name(config, add_extension = False):
    name = "unet_model_%s_%d_%d_%d" % (
            config["channels"],
            config["image_height"],
            config["image_width"],
            config["num_classes"])
    if add_extension:
        name += ".ckpt"
    return name

def restore_model_dir(models_root, config):
    model_name = restore_model_name(config)
    model_dir = os.path.join(models_root,
            model_name)

    return model_dir

def restore_model_path(models_root, config):
    model_dir = restore_model_dir(models_root, config)
    model_name_ext = restore_model_name(config, add_extension = True)

    model_path = os.path.join(model_dir,
            model_name_ext)

    return model_path

def build_model_graph(config):
    model = unet.UNet(
            shape = [
                config["image_height"],
                config["image_width"],
                unet.helpers.num_channels(config["channels"]),
            ],
            num_classes = config["num_classes"],
            num_layers = config["num_layers"],

            down_sample_rule = unet.helpers.DownSampleRule(
                config["num_root_filters"]),

            up_sample_rule = unet.helpers.UpSampleRule(
                config["num_root_filters"],
                config["num_layers"]))

    return model
