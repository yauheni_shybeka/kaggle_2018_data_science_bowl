from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import shutil

import tensorflow as tf
import numpy as np

import unet
import unet.io

import utils.logwrap

import api.unet.share


logger = utils.logwrap.get_logger(__name__)

def _check_datasets_root(datasets_root):
    logger.info("The root of datasets: %s" %\
            os.path.abspath(datasets_root))
    assert os.path.isdir(datasets_root),\
            "Invalid root of datasets"

def _check_models_root(models_root):
    logger.info("The root of models: %s" %\
            os.path.abspath(models_root))
    assert not os.path.isfile(models_root),\
            "Invalid root of models (should be a directory, not a file)"

    if not os.path.isdir(models_root):
        logger.debug("The root of models doesn't exist. Creating new one")
        os.makedirs(models_root)

def _create_dataset_reader(datasets_root, config):
    dataset_name = api.unet.share.restore_dataset_name(config)
    logger.debug("The dataset name: %s" % dataset_name)

    dataset_path = api.unet.share.restore_dataset_path(
            datasets_root,
            config)

    dataset_reader = unet.io.DatasetReader(dataset_path)
    logger.debug("The dataset loaded by path: %s",
            os.path.abspath(dataset_path))

    return dataset_reader

def _create_train_validate_view(dataset_reader, validation_size):
    dataset_indices = np.arange(dataset_reader.cardinality)

    dataset_pivot_index =\
            int(dataset_reader.cardinality * (1.0 - validation_size))

    train_view = unet.io.TrainView(dataset_reader,
            indices = dataset_indices[:dataset_pivot_index])
    logger.debug("The size of train set: %d" % train_view.cardinality)

    validate_view = unet.io.ValidateView(dataset_reader,
            indices = dataset_indices[dataset_pivot_index:])
    logger.debug("The size of validation set: %d" % validate_view.cardinality)

    return train_view, validate_view


def _build_model_graph(config):
    return api.unet.share.build_model_graph(config)

def _build_train_graph(model):
    return unet.Trainer(model)

def _build_validate_graph(model):
    return unet.metrics.Validator(model)


def _rewrite_model(sess, model_dir):
    if os.path.isdir(model_dir):
        logger.warning("Removing the previously saved model by path: %s" %\
                os.path.abspath(model_dir))
        shutil.rmtree(model_dir)
    sess.run(tf.global_variables_initializer())

    logger.info("A new model has been created")

def _preload_model(sess, model_dir):
    if os.path.isdir(model_dir):
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint(model_dir + '/.'))

        logger.info("The model has been restored by path: %s",
                os.path.abspath(model_dir))
    else:
        sess.run(tf.global_variables_initializer())

        logger.info("A new model has been created")


def _save_model(sess, model_path, global_step = None):
    saver = tf.train.Saver()
    save_path = saver.save(sess, model_path, global_step = global_step)

    logger.info("The model has been saved by path: %s",
            os.path.abspath(save_path))


def train(args, config):
    _check_datasets_root(args.datasets_root)
    _check_models_root(args.models_root)

    dataset_reader = _create_dataset_reader(args.datasets_root, config)
    train_view, validate_view =\
            _create_train_validate_view(dataset_reader, args.validation_size)

    with tf.Session(graph = tf.Graph()) as sess:
        model = _build_model_graph(config)
        trainer = _build_train_graph(model)
        validator = _build_validate_graph(model)

        model_dir = api.unet.share.restore_model_dir(
                args.models_root,
                config)

        if args.rewrite:
            _rewrite_model(sess, model_dir)
        else:
            _preload_model(sess, model_dir)

        for epoch_idx in range(args.num_epochs):
            trainer.train_epoch(sess, train_view,
                    batch_size = args.batch_size)

            val_acc, val_loss = validator.validate(sess, validate_view,
                    batch_size = args.batch_size,
                    loss_func = trainer.loss_func)

            print("Epoch: {%3d}; Loss: %0.5f; Accuracy: %0.5f" %\
                    (epoch_idx, val_loss, val_acc))

        model_path = api.unet.share.restore_model_path(
                args.models_root,
                config)
        _save_model(sess, model_path, global_step = model.global_step)
