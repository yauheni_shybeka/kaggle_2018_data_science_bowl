from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from tqdm import tqdm

import os

import unet.io
import unet.helpers

import utils.logwrap

import api.unet


logger = utils.logwrap.get_logger(__name__)

def _check_input_dir(input_dir):
    logger.info("The input dataset directory location: %s" %\
            os.path.abspath(input_dir))
    assert os.path.isdir(input_dir),\
            "Invalid input dataset directory location"

def _check_datasets_root(datasets_root):
    logger.info("The root of datasets: %s" %\
            os.path.abspath(datasets_root))
    assert not os.path.isfile(datasets_root),\
            "Invalid root of datasets (should be a directory, not a file)"

    if not os.path.isdir(datasets_root):
        logger.warning("The root of datasets doesn't exist."\
                " Creating new one...")
        os.makedirs(datasets_root)

def _create_input_reader(input_dir, config):
    input_reader = unet.io.InputReader(input_dir, config["channels"])
    return input_reader

def _create_dataset_transform(config):
    dataset_transform = unet.io.DatasetTransform(
            image_height = config["image_height"],
            image_width = config["image_width"],
            num_channels = unet.helpers.num_channels(config["channels"]),
            num_classes = config["num_classes"])

    return dataset_transform

def _apply_transform(input_reader, dataset_transform):
    image_ids = input_reader.get_ids()
    logger.debug("The number of dataset entries: %d" % len(image_ids))

    x_orig = []
    y_orig = []
    x_net = dataset_transform.x_net(len(image_ids))
    y_net = dataset_transform.y_net(len(image_ids))

    logger.info("Running original to net dataset transformation...")
    for ordinal, image_id in tqdm(enumerate(image_ids), total = len(image_ids)):
        image = input_reader.load_image(image_id)
        masks = input_reader.load_masks(image_id)

        x_image = dataset_transform.transform_image(image)
        y_masks = dataset_transform.transform_masks(masks)

        x_net[ordinal] = x_image
        y_net[ordinal] = y_masks

        x_orig.append(image)
        y_orig.append(masks)

    return ( image_ids, (x_orig, y_orig), (x_net, y_net) )

def _save_dataset(tr_data, datasets_root, config):
    ( image_ids, (x_orig, y_orig), (x_net, y_net) ) = tr_data

    dataset_name = api.unet.share.restore_dataset_name(config)

    dataset_writer = unet.io.DatasetWriter(datasets_root, dataset_name)
    dataset_writer.write(
            data_ids = image_ids,
            x_net = x_net,
            y_net = y_net,
            x_orig = x_orig,
            y_orig = y_orig)

    logger.info("The dataset has been written by path: %s" %\
            os.path.abspath(os.path.join(datasets_root, dataset_name)))
 

def preprocess(args, config):
    _check_input_dir(args.input_dir)
    _check_datasets_root(args.datasets_root)

    input_reader = _create_input_reader(args.input_dir, config)
    dataset_transform = _create_dataset_transform(config)

    tr_data = _apply_transform(input_reader, dataset_transform)
    _save_dataset(tr_data, args.datasets_root, config)
