from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import tensorflow as tf


class Trainer:

    @staticmethod
    def _get_loss_func(net):
        flat_logits = tf.reshape(net.logits, [ -1, net.num_classes ])
        flat_labels = tf.reshape(net.labels, [ -1, net.num_classes ])
        
        loss = tf.losses.softmax_cross_entropy(
                onehot_labels = flat_labels,
                logits = flat_logits)
        return loss

    @staticmethod
    def _get_optimizer(net, loss, learning_rate = 1e-4):
        optimize = tf.train.AdamOptimizer(learning_rate = learning_rate)
        optimize = optimize.minimize(loss, global_step = net.global_step)
        return optimize


    def __init__(self, net):
        self._net = net

        self._loss = Trainer._get_loss_func(net)
        self._optimize = Trainer._get_optimizer(net, self._loss)


    def train_batch(self, sess, x_train, y_train):
        feed_dict = self._net.train_feed(x_train, y_train)

        _, loss = sess.run( [ self._optimize, self._loss ],
                feed_dict = feed_dict )

        return loss

    def train_epoch(self, sess, train_set, batch_size = 1):
        train_set.reset_batch_iter()

        while train_set.next_batch(batch_size):
            x_train, y_train = train_set.get()
            loss = self.train_batch(sess, x_train, y_train)


    @property
    def loss_func(self):
        return self._loss
