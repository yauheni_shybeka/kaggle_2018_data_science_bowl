from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import tensorflow as tf
import numpy as np


def _conv(inputs, num_filters, keep_probe, kernel_size = 4, activation = tf.nn.relu, name = None):
    layer = tf.layers.conv2d(
            inputs = inputs,
            filters = num_filters,
            kernel_size = kernel_size,
            strides = 1,
            padding = "SAME",
            activation = activation,
            name = name)
    layer = tf.nn.dropout(layer, keep_probe)

    return layer

def _max_pool(inputs, name = None):
    layer = tf.layers.max_pooling2d(
            inputs, pool_size = 2, strides = 2, padding = "SAME", name = name)
    return layer

def _conv_tr(inputs, num_filters, name = None):
    layer = tf.layers.conv2d_transpose(
            inputs = inputs,
            filters = num_filters,
            kernel_size = 2,
            strides = 2,
            padding = "SAME",
            name = name)
    return layer
 
class _XUnit:
    def __init__(self, shape):
        self.x = tf.placeholder(tf.float32,
                shape = [ None, shape[0], shape[1], shape[2] ],
                name = "X")

    @property
    def out(self):
        return self.x

class _YUnit:
    def __init__(self, shape, num_classes):
        self.y = tf.placeholder(tf.float32,
                shape = [ None, shape[0], shape[1], num_classes ],
                name = "Y")

    @property
    def out(self):
        return self.y

class _RateUnit:
    def __init__(self):
        self.rate = tf.placeholder(tf.float32, name = "rate")

    @property
    def out(self):
        return self.rate


class _DownUnit:
    def __init__(self, in_unit, num_filters, rate_unit):
        self.c1 = _conv(in_unit.out, num_filters, rate_unit.out, name = "conv1")
        self.c2 = _conv(self.c1, num_filters, rate_unit.out, name = "conv2")
        self.m = _max_pool(self.c2, name = "max_pool")

    @property
    def conv_out(self):
        return self.c2

    @property
    def out(self):
        return self.m

class _MidUnit:
    def __init__(self, in_unit, num_filters, rate_unit):
        self.c1 = _conv(in_unit.out, num_filters, rate_unit.out, name = "conv1")
        self.c2 = _conv(self.c1, num_filters, rate_unit.out, name = "conv2")

    @property
    def out(self):
        return self.c2

class _UpUnit:
    def __init__(self, in_unit, down_unit, num_filters, rate_unit):
        self.tr = _conv_tr(in_unit.out, num_filters, name = "conv_tr")
        self.cc = tf.concat([self.tr, down_unit.conv_out], axis = 3)
        self.c1 = _conv(self.cc, num_filters, rate_unit.out, name = "conv1")
        self.c2 = _conv(self.c1, num_filters, rate_unit.out, name = "conv2")

    @property
    def out(self):
        return self.c2

class _OutUnit:
    def __init__(self, in_unit, num_classes):
        self.logits = _conv(in_unit.out, num_classes, 1.0, 1, activation = None, name = "conv")

    @property
    def out(self):
        return self.logits

class UNet:
    def __init__(self,
            shape,
            num_layers,
            num_classes,
            down_sample_rule,
            up_sample_rule):
        
        self._global_step = tf.Variable(0, name = "global_step", trainable = False)
        self._X = _XUnit(shape)
        self._Y = _YUnit(shape, num_classes)
        self._rate = _RateUnit()
        self._num_classes = num_classes

        down_sample_rule.reset()
        down_units_cache = []
        head_unit = self._X

        for layer_id in range(num_layers):
            with tf.variable_scope("down_unit_%d" % (layer_id + 1)):
                head_unit = _DownUnit(
                        head_unit,
                        down_sample_rule.next_num_filters(),
                        self._rate)
    
                down_units_cache.append(head_unit)

        with tf.variable_scope("mid_unit"):
            head_unit = _MidUnit(
                    head_unit,
                    down_sample_rule.next_num_filters(),
                    self._rate)

        up_sample_rule.reset()
        for layer_id in range(num_layers):
            down_unit = down_units_cache[-(layer_id + 1)]

            with tf.variable_scope("up_unit_%d" % (num_layers - layer_id)):
                head_unit = _UpUnit(
                        head_unit,
                        down_unit,
                        up_sample_rule.next_num_filters(),
                        self._rate)

        with tf.name_scope("out_unit"):
            head_unit = _OutUnit(head_unit, num_classes)
        self._logits = head_unit

    @property
    def num_classes(self):
        return self._num_classes

    @property
    def global_step(self):
        return self._global_step

    @property
    def logits(self):
        return self._logits.out

    @property
    def labels(self):
        return self._Y.out

    def train_feed(self, x, y):
        return { self._X.out : x, self._Y.out : y, self._rate.out : 0.7 }

    def validate_feed(self, x, y):
        return { self._X.out : x, self._Y.out : y, self._rate.out : 1.0 }

    def predict_feed(self, x):
        return { self._X.out : x, self._rate.out : 1.0 }
