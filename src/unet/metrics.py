from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals


import tensorflow as tf
import numpy as np
import skimage.measure


def mean_iou(original_masks, predictions):
    predicted_masks_cmb, num_predicted_masks = skimage.measure.label(
            predictions,
            connectivity = 1,
            return_num = True)

    num_original_masks = original_masks.shape[0]

    predicted_masks = np.zeros(
            shape = [
                num_predicted_masks,
                predicted_masks_cmb.shape[0],
                predicted_masks_cmb.shape[1]
            ],
            dtype = np.float32)

    for mask_id in range(num_predicted_masks):
        predicted_masks[mask_id][predicted_masks_cmb == (mask_id + 1)] = 1.0


    iou = []
    for mask_id in range(num_predicted_masks):
        best_overlap = 0.0
        best_union = 1e-9

        for orig_id in range(num_original_masks):
            overlap = predicted_masks[mask_id] * original_masks[orig_id]
            overlap_size = np.sum(overlap)
            if overlap_size > best_overlap:
                best_overlap = overlap_size
                best_union = np.sum(np.maximum(
                    predicted_masks[mask_id],
                    original_masks[orig_id]))
        
        if best_overlap == 0.0:
            iou.append(0.0)
        else:
            iou.append(best_overlap / best_union)

    p = 0.0
    for threshold in np.arange(0.5, 1.0, 0.05):
        matches = iou > threshold
        true_positive = np.count_nonzero(matches)
        false_positive = num_predicted_masks - true_positive
        false_negative = num_original_masks - true_positive
        p += true_positive / (true_positive + false_positive + false_negative)

    return p / 10 # The length of range threshold

def _get_predict(net):
    flat_logits = tf.reshape(net.logits, [ -1, net.num_classes ])

    flat_confidences = tf.nn.softmax(flat_logits, axis = 1)
    flat_predictions = tf.argmax(flat_confidences, axis = 1)

    predictions = tf.reshape(flat_predictions,
            [ -1, net.logits.shape[1], net.logits.shape[2] ])

    return predictions

class Predictor:
    def __init__(self, net):
        self._net = net
        self._predict = _get_predict(net)

    def predict(self, sess, x_net, out_size):
        feed_dict = self._net.predict_feed(x_net)
        predictions = sess.run(self._predict, feed_dict = feed_dict)

        result = np.zeros( [ x_net.shape[0], out_size[0], out_size[1] ])
        for ordinal, prediction in enumerate(predictions):
            result[ordinal] = skimage.transform.resize(prediction,
                    output_shape = out_size,
                    mode = "constant",
                    preserve_range = True)

        return result

class Validator:
    def __init__(self, net):
        self._net = net
        self._predict = _get_predict(net)

    def _validate_net_accuracy(self, y_pred, y_orig):
        y_pred_scaled = skimage.transform.resize(
                y_pred,
                output_shape = [
                    y_orig[0].shape[0],
                    y_orig[0].shape[1]
                ],
                mode = "constant",
                preserve_range = True).astype(np.int32)

        accuracy = mean_iou(y_orig, y_pred_scaled)
        return accuracy

    def validate(self, sess, validate_view,
            batch_size = 1,
            loss_func = None):

        losses = []
        accuracies = []

        validate_view.reset_batch_iter()
        while validate_view.next_batch(batch_size):
            x_net_batch, y_net_batch, y_orig_batch =\
                    validate_view.get()

            feed_dict = self._net.validate_feed(x_net_batch, y_net_batch)

            if loss_func != None:
                cur_loss, predictions = sess.run([ loss_func, self._predict ],
                        feed_dict = feed_dict)
            else:
                predictions = sess.run(self._predict, feed_dict = feed_dict)

            if loss_func != None:
                losses.append(cur_loss * len(x_net_batch))
            
            for ordinal, y_orig in enumerate(y_orig_batch):
                cur_accuracy = self._validate_net_accuracy(
                        predictions[ordinal],
                        y_orig)
                accuracies.append(cur_accuracy)

        val_acc = np.mean(accuracies)

        if loss_func != None:
            val_loss = np.sum(losses) / validate_view.cardinality
            return val_acc, val_loss
        else:
            return val_acc
