from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import shutil

import skimage.io
import skimage.transform

import numpy as np

import h5py
import json

from .helpers import num_channels


# TODO Now InputReader support only RGB channels structure
class InputReader:
    def __init__(self, input_path, channels):
        self._input_path = input_path

        self._image_pattern = "%s/{}/images/{}.png" % self._input_path
        self._masks_pattern = "%s/{}/masks/*.png" % self._input_path

        self._channels = channels

    def get_ids(self):
        return next(os.walk(self._input_path))[1]

    def load_image(self, image_id):
        image_path = self._image_pattern.format(image_id, image_id)

        image = skimage.io.imread(image_path)\
                [:,:,:num_channels(self._channels)]

        return image

    @staticmethod
    def _imread_mask(f):
        mask = skimage.io.imread(f)
        if len(mask.shape) > 2:
            mask = mask[:,:,0]
        return mask

    def load_masks(self, image_id):
        masks_path = self._masks_pattern.format(image_id)

        masks = skimage.io \
                .ImageCollection(masks_path,
                        load_func = InputReader._imread_mask) \
                .concatenate()

        return masks

class _DatasetTransform_2cls:
    NUM_CLASSES = 2

    BACKGROUND_ID = 0
    MASK_ID = 1

    def __init__(self, image_height, image_width):
        self._image_height = image_height
        self._image_width = image_width

    def normalize_image(self, source_image):
        image = skimage.transform.resize(source_image,
                output_shape = ( self._image_height, self._image_width ),
                mode = "constant",
                preserve_range = True).astype(np.float32) / 255.0

        return image

    def normalize_masks(self, source_masks):
        mask = np.zeros(
                shape = [
                    self._image_height,
                    self._image_width,
                    _DatasetTransform_2cls.NUM_CLASSES
                ],
                dtype = np.float32)

        mask[:,:, _DatasetTransform_2cls.BACKGROUND_ID] = 1.0

        for source_mask in source_masks:
            resized = skimage.transform.resize(source_mask,
                    output_shape = ( self._image_height, self._image_width ),
                    mode = "constant",
                    preserve_range = True).astype(np.float32) / 255.0

            mask[resized > 0, _DatasetTransform_2cls.BACKGROUND_ID] = 0.0
            mask[resized > 0, _DatasetTransform_2cls.MASK_ID] = 1.0

        return mask

class DatasetTransform:
    def __init__(self, image_height, image_width, num_channels, num_classes):
        assert num_classes == 2,\
                "Invalid num_classes: expected { 2 }, provided = %d" % num_classes

        self._transform = _DatasetTransform_2cls(image_height, image_width)
        self._num_channels = num_channels
        self._num_classes = num_classes

    def transform_image(self, image):
        return self._transform.normalize_image(image)

    def transform_masks(self, masks):
        return self._transform.normalize_masks(masks)

    def x_net(self, dataset_size):
        shape = [
                dataset_size,
                self._transform._image_height,
                self._transform._image_width,
                self._num_channels
        ]
        return np.zeros(shape)

    def y_net(self, dataset_size):
        shape = [
                dataset_size,
                self._transform._image_height,
                self._transform._image_width,
                self._num_classes
        ]
        return np.zeros(shape)

class DatasetWriter:
    def __init__(self, output_path, output_name):
        self._output_path = output_path
        self._output_name = output_name

    def write(self, data_ids, x_net, y_net, x_orig, y_orig):
        root_dir = os.path.join(self._output_path, self._output_name)

        if os.path.isfile(root_dir):
            os.remove(root_dir)

        if os.path.isdir(root_dir):
            shutil.rmtree(root_dir)

        os.makedirs(root_dir)

        h5_path = os.path.join(root_dir, self._output_name + ".h5")
        meta_path = os.path.join(root_dir, self._output_name + ".json")

        with h5py.File(h5_path, "w") as h:
            h.create_dataset("x_net", data = x_net)
            h.create_dataset("y_net", data = y_net)

            x_orig_grp = h.create_group("x_orig")
            for ordinal, x in enumerate(x_orig):
                x_orig_grp.create_dataset(data_ids[ordinal], data = x)

            y_orig_grp = h.create_group("y_orig")
            for ordinal, y in enumerate(y_orig):
                y_orig_grp.create_dataset(data_ids[ordinal], data = y)

        with open(meta_path, "w") as f:
            f.write(json.dumps(data_ids))

class DatasetReader:
    def _preload(self):
        dataset_name = os.path.basename(self._root_path)

        h5_path = os.path.join(self._root_path, dataset_name + ".h5")
        meta_path = os.path.join(self._root_path, dataset_name + ".json")

        with open(meta_path, "r") as f:
            self._data_ids = json.loads(f.read())

        with h5py.File(h5_path, "r") as h:
            self._x_net = h["x_net"][:]
            self._y_net = h["y_net"][:]

            self._x_orig = dict()
            self._y_orig = dict()

            for image_id in self._data_ids:
                self._x_orig[image_id] = h["x_orig"][image_id][:]
                self._y_orig[image_id] = h["y_orig"][image_id][:]

    def __init__(self, root_path):
        self._root_path = root_path
        self._preload()

    @property
    def cardinality(self):
        return len(self._data_ids)

    def get_x_net(self, indices):
        return self._x_net[indices]

    def get_y_net(self, indices):
        return self._y_net[indices]

    def get_y_orig(self, indices):
        y_orig = []

        for index in indices:
            image_id = self._data_ids[index]
            masks = self._y_orig[image_id]

            y_orig.append(masks)

        return y_orig

class DatasetView(object):
    def __init__(self, dataset_reader, indices):
        self._dataset_reader = dataset_reader
        self._indices = indices

        self.reset_batch_iter()

    def reset_batch_iter(self):
        self._start_iter = -1
        self._end_iter = 0

    def next_batch(self, batch_size):
        if self._start_iter >= len(self._indices):
            return False

        self._start_iter = self._end_iter
        self._end_iter = min(len(self._indices), self._end_iter + batch_size)

        if self._start_iter >= len(self._indices):
            return False

        return True
    
    @property
    def cardinality(self):
        return len(self._indices)

class TrainView(DatasetView):
    def __init__(self, dataset_reader, indices):
        super(TrainView, self).__init__(dataset_reader, indices)

    def get(self):
        x_batch = self._dataset_reader\
                .get_x_net(self._indices[self._start_iter:self._end_iter])
        y_batch = self._dataset_reader\
                .get_y_net(self._indices[self._start_iter:self._end_iter])

        return x_batch, y_batch

class ValidateView(DatasetView):
    def __init__(self, dataset_reader, indices):
        super(ValidateView, self).__init__(dataset_reader, indices)

    def get(self):
        x_batch = self._dataset_reader\
                .get_x_net(self._indices[self._start_iter:self._end_iter])
        y_batch = self._dataset_reader\
                .get_y_net(self._indices[self._start_iter:self._end_iter])

        y_orig_batch = self._dataset_reader\
                .get_y_orig(self._indices[self._start_iter:self._end_iter])

        return x_batch, y_batch, y_orig_batch
