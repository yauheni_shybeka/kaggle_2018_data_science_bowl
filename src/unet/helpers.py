from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals


def num_channels(channels):
    if channels == "RGB":
        return 3
    else:
        raise ValueError("Unknown channels structure: %s" % channels)

class DownSampleRule:
    def __init__(self, num_root_filters):
        self.num_root_filters = num_root_filters
        self.num_filters = -1

    def reset(self):
        self.num_filters = self.num_root_filters

    def next_num_filters(self):
        res = self.num_filters
        self.num_filters = self.num_filters * 2
        return res

class UpSampleRule:
    def __init__(self, num_root_filters, num_layers):
        self.num_root_filters = num_root_filters
        self.num_layers = num_layers

    def reset(self):
        self.num_filters = self.num_root_filters *\
                (2 ** self.num_layers)

    def next_num_filters(self):
        res = self.num_filters
        self.num_filters = self.num_filters // 2 
        return res
