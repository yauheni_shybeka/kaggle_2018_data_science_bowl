from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import logging


def get_logger(name, root = False):
    FORMAT = u"%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s"

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    
    if root:
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter(FORMAT)
        ch.setFormatter(formatter)
        
        logger.addHandler(ch)

    return logger
