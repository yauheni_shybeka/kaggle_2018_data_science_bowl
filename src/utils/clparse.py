from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import argparse


def _create_preprocess_parser(subparsers, preprocess_command):
    parser = subparsers.add_parser("preprocess",
            help = "Preprocess an input dataset")

    # required arguments
    required = parser.add_argument_group("required named arguments")

    required.add_argument("--input-dir",
            required = True,
            help = "Set a dataset root directory location")

    required.add_argument("--config-file",
            required = True,
            help = "Set a model config file location")

    required.add_argument("--datasets-root",
            required = True,
            help = "Set a preprocessed datasets root directory location."\
                    " The final location will be generated base on this "\
                    " and model config parameters")

    # parser redirect
    parser.set_defaults(command = preprocess_command)

def _create_train_parser(subparsers, train_command):
    parser = subparsers.add_parser("train",
            help = "Train a model")

    # required arguments
    required = parser.add_argument_group("required named arguments")

    required.add_argument("--datasets-root",
            required = True,
            help = "Set a preprocessed datasets root directory location."\
                    " The final location will be generated base on this "\
                    " and model config parameters")

    required.add_argument("--config-file",
            required = True,
            help = "Set a model config file location")

    required.add_argument("--models-root",
            required = True,
            help = "Set a models root directory location."\
                    " The final location will be generated base on this "\
                    " and model config parameters")

    # optional arguments
    optional = parser.add_argument_group("optional named arguments")

    optional.add_argument("--validation-size",
            default = 0.05,
            type = float,
            help = "Set a validation part relative size")

    optional.add_argument("--batch-size",
            default = 8,
            type = int,
            help = "Set the batch size of training process")

    optional.add_argument("--num-epochs",
            default = 1,
            type = int,
            help = "Set the number of training epochs")

    optional.add_argument("--rewrite",
            action = "store_true",
            help = "Set if the previosly saved model should be rewritten")

    # parser redirect
    parser.set_defaults(command = train_command)

def _create_predict_parser(subparsers, predict_command):
    parser = subparsers.add_parser("predict",
            help = "Perform predections")

    # required arguments
    required = parser.add_argument_group("required named arguments")

    required.add_argument("--input-dir",
            required = True,
            help = "Set a dataset root directory location")

    required.add_argument("--config-file",
            required = True,
            help = "Set a model config file location")

    required.add_argument("--models-root",
            required = True,
            help = "Set a models root directory location."\
                    " The final location will be generated base on this"\
                    " and model config parameters")

    required.add_argument("--dump-file",
            required = True,
            help = "Set an output dump csv file name for Kaggle."\
                    " The final location will be generated base on this"\
                    " and model config parameters"\
                    " Warning (Don't add extension, it will be added"\
                    " automatically)")

    # parser redirect
    parser.set_defaults(command = predict_command)

def parse_args(
        preprocess_command,
        train_command,
        predict_command):

    parser = argparse.ArgumentParser(prog = "Kaggle Nuclei Solver")
    parser.set_defaults(command = None)

    subparsers = parser.add_subparsers(help = "commands")
    _create_preprocess_parser(subparsers, preprocess_command)
    _create_train_parser(subparsers, train_command)
    _create_predict_parser(subparsers, predict_command)

    args = parser.parse_args()
    return args
