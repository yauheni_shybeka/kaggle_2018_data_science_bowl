set GIT_REP_NAME=kaggle-dsbowl-2018-dataset-fixes
set GIT_REP_URL=https://github.com/lopuhin/%GIT_REP_NAME%.git

set OUT_DIR=..\stage1_train

echo "Git fixes repository:" %GIT_REP_URL%

if EXIST %OUT_DIR% (
  cd %GIT_REP_NAME%
  git pull
  cd ..
) ELSE (
  git clone %GIT_REP_URL%
)

rmdir %OUT_DIR% /S /Q
xcopy kaggle-dsbowl-2018-dataset-fixes\stage1_train %OUT_DIR% /s /e /i /Y
