#!/bin/sh

GIT_REP_NAME="kaggle-dsbowl-2018-dataset-fixes"
GIT_REP_URL="https://github.com/lopuhin/""$GIT_REP_NAME"".git"

OUT_DIR="./stage1_train"

echo "Git fixes repository:" $GIT_REP_URL

if [ -d "$GIT_REP_NAME" ]; then
	cd "$GIT_REP_NAME"
	git pull
	cd ..
else
	git clone "$GIT_REP_URL"
fi

rm -rf "$OUT_DIR"
cp kaggle-dsbowl-2018-dataset-fixes/stage1_train -r ./stage1_train
