python "../src/main.py" train --datasets-root "../cache/datasets" --config-file "./configs/unet.json" --models-root "../cache/models" --num-epochs 10 --batch-size 16
